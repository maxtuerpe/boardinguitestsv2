let rowNum = 2;
class Application {
    constructor(){
        this.statusCircles = [];
    };
    status(){
        let status = "Green";
        for(let i = 0; i < this.statusCircles.length; i++){
            switch(this.statusCircles[i]){
                case "red":
                    status = "Red";
                    return status;
                case "yellow":
                    status = "Yellow";
                    break;
            }
        }
        return status;
    }
}
const getRowDataUWF = (client, expectValue) => {
    client.element('xpath', `//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]`, (res) => {
        const app = new Application()
        let attributeNum = 0;
        const getAttributeData = (result) => {
            switch(result.value) {
                case "count green fa fa-check":
                    app.statusCircles.push("green");
                    break;
                case "count yellow":
                    app.statusCircles.push("yellow");
                    break;
                case "count red":
                case "count harddecline":
                    app.statusCircles.push("red");
                    break;  
            }
            attributeNum++
            if(attributeNum === 4){
                const finalStatus = app.status()
                client.assert.equal(finalStatus, expectValue)
            }
        }
        // The res.value only has a message when there is not an element found using the path.
        // This will reccur until there are no more rows.
        if (!res.value.message){ 
            for (let i = 0; i < 4; i++){
                client.getAttribute(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[${i + 3}]/span`, 'class', getAttributeData)
            }
            rowNum++;
            getRowDataUWF(client, expectValue);
        }
    })   
}

const testUWFilter = (buttonName, filterColor, client) => {
    client
        .useXpath()
        .waitForElementNotVisible('//div[@id="CBLoading"]')
        .click(`//span[@class="fa ${buttonName}"]`)
        .waitForElementNotVisible('//div[@id="CBLoading"]', function(){
            rowNum = 2;
            getRowDataUWF(client, filterColor);
        })
        .click(`//span[@class="fa ${buttonName}"]`)
}
// BUTTON NAMES
    // fa-check-circle
    // fa-exclamation-circle
    // fa-times-circle






const getRowDataASF = (client, expectValue) => {
    const getAttributeData = (result) => {
        let finalStatus = "";
        let camelCasedValue = result.value.replace(/-| /g, '');
        switch(camelCasedValue){
            case "PreGenerated":
                finalStatus = "PreGenerated";
                break;
            case "Processing":
                finalStatus = "Processing";
                break;
            case "Triggering":
                finalStatus = "Triggering";
                break;
            case "Incomplete":
                finalStatus = "Incomplete";
                break;
            case "NewPend":
                finalStatus = "NewPend";
                break;
            case "FollowUp":
                finalStatus = "FollowUp";
                break;
            case "RePend":
                finalStatus = "RePend";
                break;
            case "Triggered":
                finalStatus = "Triggered";
                break;
            case "AcquirerReview":
                finalStatus = "AcquirerReview";
                break;
            case "Approved":
                finalStatus = "Approved";
                break;
            case "ApprovedAndBoarded":
                finalStatus = "ApprovedAndBoarded";
                break;
            case "Declined":
                finalStatus = "Declined";
                break;
        }
        client.assert.equal(finalStatus, expectValue)
    }
    client.element('xpath', `//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]`, (res) => {
        // The res.value only has a message when there is not an element found using the path.
        // This will reccur until there are no more rows.
        if (!res.value.message){ 
            client.getAttribute(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[2]/a/div/span`, 'title', getAttributeData)
            rowNum++;
            getRowDataASF(client, expectValue);
        }
    })   
}
const testApplicationStatus = (value, client) => {
    client
        .useXpath()
        .waitForElementNotVisible('//div[@id="CBLoading"]')
        .click(`//input[@value="${value}"]`)
        .waitForElementNotVisible('//div[@id="CBLoading"]', function(){
            rowNum = 2;
            getRowDataASF(client, `${value}`);
        })
        .click(`//input[@value="${value}"]`)
}



class Column {
    constructor(){
        this.statusCircles = [];
    };
    status(){
        let status = "Red";
        for(let i = 0; i < this.statusCircles.length; i++){
            if(this.statusCircles[i] === "green"){
                status = "Green";  
            }
        }
        return status;
    }
}
const getColumnData = (client, column, colName) => {
    let colNum = 0;
    switch(colName){
        case "KYC":
            colNum = 3;
            break;
        case "FraudCredit":
            colNum = 4;
            break;
        case "AcceptableUse":
            colNum = 5;
            break;
    }
    client.element('xpath', `//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]`, (res) => {
        const getAttributeData = (result) => {
            switch(result.value) {
                case "count green fa fa-check":
                    column.statusCircles.push("green");
                    break;
                case "count yellow":
                case "count red":
                case "count harddecline":
                    column.statusCircles.push("red");
                    break;  
            }
        }
        // The res.value only has a message when there is not an element found using the path.
        // This will reccur until there are no more rows.
        if (!res.value.message){ 
            client.getAttribute(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[${colNum}]/span`, 'class', getAttributeData)
            rowNum++;
            getColumnData(client, column, colName);
        } else {
            const finalStatus = column.status();
            client.assert.equal(finalStatus, "Red")
        }
    })   
}
const testColumn = (colName, client) => {
    client
        .useXpath()
        .waitForElementNotVisible('//div[@id="CBLoading"]')
        .click(`//input[@name="${colName}"]`)
        .waitForElementNotVisible('//div[@id="CBLoading"]', function(){
            rowNum = 2;
            const column = new Column();
            getColumnData(client, column, colName);
        })
        .click(`//input[@name="${colName}"]`)
}

module.exports = {
	'Step 1: Get to cobrand': function(client) {
    	client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
    },

    'Step 2: unclicking defaults': function(client){
        client
            .useXpath()
            .click('//span[@class="fa fa-check-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//span[@class="fa fa-exclamation-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//span[@class="fa fa-times-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="NewPend"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="FollowUp"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="RePend"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="Triggered"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="AcquirerReview"]')
    },

    'Step 3: UW filters': function(client){
        testUWFilter("fa-check-circle", "Green", client);
        testUWFilter("fa-exclamation-circle", "Yellow", client);
        testUWFilter("fa-times-circle", "Red", client);
    },

    'Step 4: Application status filters': function(client){
        testApplicationStatus("PreGenerated", client)
        testApplicationStatus("Processing", client)
        testApplicationStatus("Triggering", client)
        testApplicationStatus("Incomplete", client)
        testApplicationStatus("NewPend", client)
        testApplicationStatus("FollowUp", client)
        testApplicationStatus("RePend", client)
        testApplicationStatus("Triggered", client)
        testApplicationStatus("AcquirerReview", client)
        testApplicationStatus("Approved", client)
        testApplicationStatus("ApprovedAndBoarded", client)
        testApplicationStatus("Declined", client)
    },

    'Step 5: Underwriting Dashboard Status Filters': function(client){
        testColumn("KYC", client);
        testColumn("FraudCredit", client);
        testColumn("AcceptableUse", client);
    },
    
    'Step 6: pausing and ending': function(client){
        client
            .pause(300)
            .end()
    }
}