// This test enters multiple values into the search bar and asserts that application with matching strings are returned to the table
const testingValue = (client, entryValue) => {
	client
		.useXpath()
		.setValue('//input[@placeholder="Search Apps, Names, etc."]', entryValue)
		.waitForElementPresent('//div[@class="results-box loadingBG"]')
		.waitForElementVisible('//div[@id="searchresults"]/div/table/tbody/tr[2]/td[2]/span', Infinity)
		.getText('//div[@id="searchresults"]/div/table/tbody/tr[2]/td[2]/span', function(res){
			const compare = res.value.toLowerCase();
			entryValue = entryValue.toLowerCase();
			client.assert.equal(compare, entryValue)
		})
		.clearValue('//input[@placeholder="Search Apps, Names, etc."]')	
}

module.exports = {
	'Step 1: Get to cobrand': function(client) {
    	client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
	},
	'Step 2: Searching for first value' : function(client){
		const testValues = ['Max Makes', "bad", 'WILD', "FeLiNe"]
		testingValue(client, testValues[0])
		testingValue(client, testValues[1])
		testingValue(client, testValues[2])
		testingValue(client, testValues[3])
	},

	'Pausing and ending': function(client) {
		client
			.pause(2000)
			.end()
	}	
}
        