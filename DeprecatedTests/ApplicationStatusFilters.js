// This test checks all of the application status filters one at atime and asserts that the table is only populated by the respective and correct applications.

let rowNum = 2;
const getRowDataASF = (client, expectValue) => {
    const getAttributeData = (result) => {
        let finalStatus = "";
        let camelCasedValue = result.value.replace(/-| /g, '');
        switch(camelCasedValue){
            case "PreGenerated":
                finalStatus = "PreGenerated";
                break;
            case "Processing":
                finalStatus = "Processing";
                break;
            case "Triggering":
                finalStatus = "Triggering";
                break;
            case "Incomplete":
                finalStatus = "Incomplete";
                break;
            case "NewPend":
                finalStatus = "NewPend";
                break;
            case "FollowUp":
                finalStatus = "FollowUp";
                break;
            case "RePend":
                finalStatus = "RePend";
                break;
            case "Triggered":
                finalStatus = "Triggered";
                break;
            case "AcquirerReview":
                finalStatus = "AcquirerReview";
                break;
            case "Approved":
                finalStatus = "Approved";
                break;
            case "ApprovedAndBoarded":
                finalStatus = "ApprovedAndBoarded";
                break;
            case "Declined":
                finalStatus = "Declined";
                break;
        }
        client.assert.equal(finalStatus, expectValue)
    }
    client.element('xpath', `//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]`, (res) => {
        // The res.value only has a message when there is not an element found using the path.
        // This will reccur until there are no more rows.
        if (!res.value.message){ 
            client.getAttribute(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[2]/a/div/span`, 'title', getAttributeData)
            rowNum++;
            getRowData(client, expectValue);
        }
    })   
}
const testValue = (value, client) => {
    client
        .useXpath()
        .waitForElementNotVisible('//div[@id="CBLoading"]')
        .click(`//input[@value="${value}"]`)
        .waitForElementNotVisible('//div[@id="CBLoading"]', function(){
            rowNum = 2;
            getRowData(client, `${value}`);
        })
        .click(`//input[@value="${value}"]`)
}

module.exports = {
	'Step 1: Get to cobrand': function(client) {
    	client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
    },

    'Step 2: unclicking defaults': function(client){
        client
            .useXpath()
            .click('//span[@class="fa fa-check-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//span[@class="fa fa-exclamation-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//span[@class="fa fa-times-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="NewPend"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="FollowUp"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="RePend"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="Triggered"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="AcquirerReview"]')
    },

    'Step 3: getting & testing data': function(client){
        testValue("PreGenerated", client)
        testValue("Processing", client)
        testValue("Triggering", client)
        testValue("Incomplete", client)
        testValue("NewPend", client)
        testValue("FollowUp", client)
        testValue("RePend", client)
        testValue("Triggered", client)
        testValue("AcquirerReview", client)
        testValue("Approved", client)
        testValue("ApprovedAndBoarded", client)
        testValue("Declined", client)
    },
    
    'Step 4: pausing and ending': function(client){
        client
            .pause(300)
            .end()
    }
}