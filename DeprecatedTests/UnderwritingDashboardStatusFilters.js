let rowNum = 2;
class Column {
    constructor(){
        this.statusCircles = [];
    };
    status(){
        let status = "Red";
        for(let i = 0; i < this.statusCircles.length; i++){
            if(this.statusCircles[i] === "green"){
                status = "Green";  
            }
        }
        return status;
    }
}
const getColumnData = (client, column, colName) => {
    let colNum = 0;
    switch(colName){
        case "KYC":
            colNum = 3;
            break;
        case "FraudCredit":
            colNum = 4;
            break;
        case "AcceptableUse":
            colNum = 5;
            break;
    }
    client.element('xpath', `//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]`, (res) => {
        const getAttributeData = (result) => {
            switch(result.value) {
                case "count green fa fa-check":
                    column.statusCircles.push("green");
                    break;
                case "count yellow":
                case "count red":
                case "count harddecline":
                    column.statusCircles.push("red");
                    break;  
            }
        }
        // The res.value only has a message when there is not an element found using the path.
        // This will reccur until there are no more rows.
        if (!res.value.message){ 
            client.getAttribute(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[${colNum}]/span`, 'class', getAttributeData)
            rowNum++;
            getColumnData(client, column, colName);
        } else {
            const finalStatus = column.status();
            client.assert.equal(finalStatus, "Red")
        }
    })   
}
const testColumn = (colName, client) => {
    client
        .useXpath()
        .waitForElementNotVisible('//div[@id="CBLoading"]')
        .click(`//input[@name="${colName}"]`)
        .waitForElementNotVisible('//div[@id="CBLoading"]', function(){
            rowNum = 2;
            const column = new Column();
            getColumnData(client, column, colName);
        })
        .click(`//input[@name="${colName}"]`)
}


module.exports = {
	'Step 1: Get to cobrand': function(client) {
    	client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
    },

    'Step 2: unclicking defaults': function(client){
        client
            .useXpath()
            .click('//span[@class="fa fa-check-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//span[@class="fa fa-exclamation-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//span[@class="fa fa-times-circle"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="NewPend"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="FollowUp"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="RePend"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="Triggered"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//input[@value="AcquirerReview"]')
    },

    'Step 3: getting & testing data': function(client){
        testColumn("KYC", client);
        testColumn("FraudCredit", client);
        testColumn("AcceptableUse", client);
    },
    
    'Step 4: pausing and ending': function(client){
        client
            .pause(300)
            .end()
    }
}