const dataEuwids= [518, 528, 530, 539, 544, 548, 527, 531, 535, 545, 549, 519, 529, 533, 534, 540, 542, 550, 532, 547, 525, 517, 520, 521, 522, 523, 524, 526, 541];

const randomUWServiceID = dataEuwids[Math.floor(Math.random() * dataEuwids.length)] 

let rowNum = 2;

const findClaimedApp = (client) => {
	client.useXpath().getText(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[7]`, (res)=> {
		if(res.value !== "test_admin"){
			rowNum++;
			findClaimedApp(client);
		} else client.useXpath().click(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[2]/a`)
	}) 
}

const newNote = () => {
    let note = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < 10; i++){
        note += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return note;
}
const note = newNote();




module.exports ={
	'Step 1: Get to cobrand': (client) => {
		client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
		findClaimedApp(client);
    },
    'Step 2: Add notes, and check that they are OK': (client) => {
        client
            .useXpath()
            .click('//a[@href="#notes-tab"]')
            .waitForElementVisible('//input[@placeholder="Enter new note here..."]')
            .setValue('//input[@placeholder="Enter new note here..."]' , `This is a general note: ${note}`)
            .click('//input[@value="Submit"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]', Infinity)
            .getText('//div[@class="notes-holder"]/div[2]/div[2]/div/span[2]', (res) => {
                client.assert.equal(res.value, `This is a general note: ${note}`)
            })
            .click(`//a[@id="default-newnote-drop-open"]`)
            .waitForElementVisible(`//ul[@class="drop-nav"]/li/a[@data-euwid="${randomUWServiceID}"]`, Infinity)
            .click(`//ul[@class="drop-nav"]/li/a[@data-euwid="${randomUWServiceID}"]`)
            .setValue('//input[@placeholder="Enter new note here..."]' , `This is a service specific note: ${note}`)
            .click('//input[@value="Submit"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]', Infinity)
            .getText('//div[@class="notes-holder"]/div[2]/div[2]/div/span[2]', (res) => {
                client.assert.equal(res.value, `This is a service specific note: ${note}`)
            })
            .click('//a[@href="#tab1"]')
            .waitForElementVisible(`//ul[@class="data-list"]/li/a[@data-euwid="${randomUWServiceID}"]`, Infinity)
            .click(`//ul[@class="data-list"]/li/a[@data-euwid="${randomUWServiceID}"]`)
            .waitForElementNotVisible('//div[@id="CBLoading"]', Infinity)
            .click(`//a[@href="#euwchk-${randomUWServiceID}-notes-tab"]`)
            .waitForElementVisible(`//div[@id="euwchk-${randomUWServiceID}-notes-tab"]/div[@class="notes-holder"]/div[2]/div[@class="txt-area"]/div/span[2]`)
            .getText(`//div[@id="euwchk-${randomUWServiceID}-notes-tab"]/div[@class="notes-holder"]/div[2]/div[@class="txt-area"]/div/span[2]`, (res) => {
                client.assert.equal(res.value, `This is a service specific note: ${note}`)
            })

    },
    'Step 3: pausing and ending': (client) => {
        client
            .pause(300)
            .end()
    }
}