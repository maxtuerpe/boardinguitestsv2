let rowNum = 2;
const findClaimedApp = (client) => {
	client.useXpath().getText(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[7]`, (res)=> {
		if(res.value !== "test_admin"){
			rowNum++;
			findClaimedApp(client);
		} else client.useXpath().click(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[2]/a`)
	}) 
}

const waitForAlert = (client) => {
	let secondsTillAlert = 0;
	client
		.useXpath()
		.pause(1000)
		.getAlertText((res) => {
			if(res.status !== 0){
				waitForAlert(client)
				secondsTillAlert++;
			} else {
				client.acceptAlert();
				console.log(secondsTillAlert)
				return;
			}
	})
}


const openCheckClose = (client, uwid) => {
	client
		.useXpath()
		.waitForElementVisible(`//ul[@class="data-list"]/li/a[@data-euwid="${uwid}"]`)
		.click(`//ul[@class="data-list"]/li/a[@data-euwid="${uwid}"]`)
		.waitForElementNotVisible('//div[@id="CBLoading"]', Infinity)
		.isVisible(`//a[@class="close uwdetails-opener"]`)
		.click(`//a[@class="close uwdetails-opener"]`)
		.waitForElementNotVisible('//div[@id="CBLoading"]', Infinity)
}



module.exports ={
	'Step 1: Get to cobrand': (client) => {
		client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
		findClaimedApp(client);
	},
	
    'Step 2: Click things, run checks': (client) => {
		const dataEuwids= [518, 528, 530, 539, 544, 548, 527, 531, 535, 545, 549, 519, 529, 533, 534, 540, 542, 550, 532, 547, 525, 517, 520, 521, 522, 523, 524, 526, 541];
		for(let i = 0; i < dataEuwids.length; i++){
			const uwid = dataEuwids[i];
			openCheckClose(client, uwid);
		}		
	},
	
    'Step whatever: pausing and ending': (client) => {
        client
            .pause(10000)
            .end()
    }
}