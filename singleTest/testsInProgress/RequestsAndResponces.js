const path = require('path');

const newRequest = () => {
    let note = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < 10; i++){
        note += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return note;
}
const request = newRequest();

let rowNum = 2;

const findClaimedApp = (client) => {
	client.useXpath().getText(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[7]`, (res)=> {
		if(res.value !== "test_admin"){
			rowNum++;
			findClaimedApp(client);
		} else client.useXpath().click(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[2]/a`)
	}) 
}

module.exports ={
	'Step 1: Get to cobrand': (client) => {
		client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
		findClaimedApp(client);
	},

	'step 2: adding requsests': (client) => {
		client
			.useXpath()
			.click('//a[@href="#correspondence-tab"]')
			.waitForElementVisible('//textarea[@id="-submit-request-question"]')
			.setValue('//textarea[@id="-submit-request-question"]', `Requesting: ${request}`)
			.click('//button[@id="-upload-requestdoc-button"]')
			.pause(5000)
			.setValue('input[type="file"]', path.resolve(__dirname + '/LP4.jpg.')) // Works
			// .click('//input[@id="-submit-request-button"]')
			// .waitForElementNotVisible('//div[@id="CBLoading"]', Infinity)
			// .getText('//div[@class="notes-holder"]/div[2]/div/div[2]/div/span[2]', (res) => {
            //     client.assert.equal(res.value, `Requesting: ${request}`)
            // })
	}, 
	
	'Step 3: pausing and ending': (client) => {
        client
            .pause(10000)
            .end()
    }

}