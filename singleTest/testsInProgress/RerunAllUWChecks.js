let rowNum = 2;
const findClaimedApp = (client) => {
	client.useXpath().getText(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[7]`, (res)=> {
		if(res.value !== "test_admin"){
			rowNum++;
			findClaimedApp(client);
		} else client.useXpath().click(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[2]/a`)
	}) 
}

const waitForAlert = (client) => {
	let secondsTillAlert = 0;
	client
		.useXpath()
		.pause(1000)
		.getAlertText((res) => {
			if(res.status !== 0){
				waitForAlert(client)
				secondsTillAlert++;
			} else {
				client.acceptAlert();
				console.log(secondsTillAlert)
				return;
			}
		})
}


module.exports ={
	'Step 1: Get to cobrand': function(client) {
		client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
		findClaimedApp(client);
	},

	'Step 2: Hit buttons and assert stuff': function(client) {
		client
			.useXpath()
			.waitForElementNotVisible('//div[@id="CBLoading"]')
			.click('//a[@id="run-all-auto-checks"]')
			.getAlertText((res) => {
				console.log(res)
			})
			.acceptAlert()
			.getText('//div[@id="CBLoadingMsg"]', (res) => {
				client.assert.equal(res.value, "Starting tasks for all auto checks...")
			})
		waitForAlert(client)
		client
			.useXpath()
			.getAttribute('//div[@class="col-holder"]/div[1]/')
	},
	'Step 3: Pausing and ending': function(client) {
		client
			.pause(300)
			.end()
	}
}