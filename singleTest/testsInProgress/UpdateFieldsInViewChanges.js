// this test goes into the view changes popup on a claimed application and changes the value of legal business name. It then asserts that the UI updates the name.


let rowNum = 2;
const findClaimedApp = (client) => {
    client.useXpath().getText(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[7]`, (res)=> {
        if(res.value !== "test_admin"){
            rowNum++;
            findClaimedApp(client);
        } else client.useXpath().click(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[2]/a`)
    }) 
}
const newName = () => {
    let name = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < 10; i++){
        name += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return name;
}
const name = newName();

module.exports = {
	'Step 1: Get to cobrand': function(client) {
    	client
			.url('https://dev.merchantapp.io/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]')
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]')
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
            .click('a[href="/klaus/CoBrands/Details"]')
    },

    'Step 2: clicking buttons/ running tests': (client) => {
        findClaimedApp(client);
        client 
            .useXpath()
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//a[@id="update-info-merchant"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .clearValue('//input[@data-shortname="Legal Name"]')
            .setValue('//input[@data-shortname="Legal Name"]', name)
            .click('//button[@class="MerchantDetails-SaveAndClose btn btn-info"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .waitForElementVisible('//h1[@id="legal-name-txt"]')
            .pause(1000)
            .getText('//h1[@id="legal-name-txt"]', (res) => {
                client.assert.equal(res.value, name)
            })
    },

    'Step 3: pausing and ending': function(client){
        client
            .pause(300)
            .end()
    }
}