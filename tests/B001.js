// this test makes sure OpenID works.
module.exports = {
    'Demo test': function (browser) {
      browser
        .url('https://boardingwebapptest.azurewebsites.net/system/UserAccount/Login')
        .waitForElementVisible('input[name=Email]', 1000)
        .setValue('input[name=Email]', 'testadmin@infinicept.com')
        .click('button[name=submitButton]')
        .waitForElementVisible('input[name=Password]', 1000)
        .setValue('input[name=Password]', 'vRnK$W7C1cr5')
        .click('button[class="btn btn-primary"]')
        .pause(300)
        .end()
    }
}

