// This tests OpenID for a cobrand portal
module.exports = {
    'Demo test': function (browser) {
      browser
        .url('https://boardingwebapptest.azurewebsites.net/klaus/UserAccount/Login')
        .waitForElementVisible('input[name=Email]', 1000)
        .setValue('input[name=Email]', 'B002@infinicept.com')
        .click('button[name=submitButton]')
        .waitForElementVisible('input[name=Password]', 1000)
        .setValue('input[name=Password]', 'x48Pf9*2bi8w')
        .click('button[class="btn btn-primary"]')
        .pause(300)
        .end()
    }
}

