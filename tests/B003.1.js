// This test asserts that you can't register an email theat already has an account associated with it.
module.exports = {
    'Demo test': function (browser) {
      browser
        .url('https://boardingwebapptest.azurewebsites.net/klaus/UserAccount/Login')
        .waitForElementVisible('input[name=Email]', 1000)
        .setValue('input[name=Email]', 'B003@infinicept.com')
        .click('button[tabindex="8"]')
        .assert.containsText('li', 'An account with that email address is already registered, please sign in.')
        .pause(300)
        .end()
    }
}



