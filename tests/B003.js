// This tests the ability to register a new mercant account.

const newName = () => {
  let name = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  for (let i = 0; i < 10; i++){
      name += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return name;
}
const name = newName();
module.exports = {
    'Demo test': function (browser) {
      browser
        .url('https://boardingwebapptest.azurewebsites.net/klaus/UserAccount/Login')
        .waitForElementVisible('input[name=Email]', 1000)
        .setValue('input[name=Email]', `${name}@infinicept.com`)
        .click('button[tabindex="8"]')
        .setValue('input[name="Password"]', `z#5$H3hD!ZKo`)
        .setValue('input[name="ConfirmPassword"]', `z#5$H3hD!ZKo`)
        .click('button[name="redirectToBoarding"]')
        .pause(300)
        .end()
    }
}

