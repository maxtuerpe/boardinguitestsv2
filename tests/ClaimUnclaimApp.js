// this test claims and unclaims an application, and asserts the the UI updates accordingly.


let rowNum = 2;
const findUnclaimedApp = (client) => {
    client.useXpath().getText(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[7]`, (res)=> {
        if(res.value !== "UNCLAIMED"){
            rowNum++;
            findUnclaimedApp(client);
        } else client.useXpath().click(`//table[@id="DetailsListTable"]/tbody/tr[${rowNum}]/td[2]/a`)
    }) 
}


module.exports = {
	'Step 1: Get to cobrand': function(client) {
    	client
			.url('https://boardingwebapptest.azurewebsites.net/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]')
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]')
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
            .click('a[href="/klaus/CoBrands/Details"]')
    },

    'Step 2: clicking buttons/ running tests': (client) => {
        findUnclaimedApp(client);
        client 
            .useXpath()
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//a[@id="claim-this-merchant"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .waitForElementVisible('//span[@id="claimed-by-txt"]')
            .getText('//span[@id="claimed-by-txt"]', (res) => {
                client.assert.equal(res.value, "Claimed by: test_admin")
            })
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .click('//a[@name="remove-claim-from-merchant"]')
            .acceptAlert()
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .pause(300)
            .isVisible('//span[@id="claimed-by-txt"]', (res) => {
                client.assert.equal(res.value, false);
            })
    },
    'Step 3: pausing and ending': function(client){
        client
            .pause(300)
            .end()
    }
}