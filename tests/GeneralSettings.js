const randomString = () => {
    let note = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < 10; i++) {
        note += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return note;
}
const testBatchID = randomString();
const newPhoneNumber = () => {
    let number = "303";
    const possible = "0123456789";
    for (let i = 0; i < 7; i++) {
        number += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return number;
}
const testPhoneNumber = newPhoneNumber();


module.exports = {
	'Step 1: Get to cobrand settings': function(client) {
    	client
			.url('https://boardingwebapptest.azurewebsites.net/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]')
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]')
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
            .click('a[href="/klaus/CoBrands/Details"]')
            .click('a[href="/klaus/CoBrands/Edit"]')      
    },

    'Step 2: Update Info': (client) => {
        client
            .useXpath()
            .clearValue('//input[@id="Name"]')
            .setValue('//input[@id="Name"]', testBatchID)
            .clearValue('//textarea[@id="PricingHTML"]')
            .setValue('//textarea[@id="PricingHTML"]', `<h1 id="pricing-html">${testBatchID}</h1>`)
            .clearValue('//input[@id="SupportPhone"]')
            .setValue('//input[@id="SupportPhone"]', testPhoneNumber)
            .click('//input[@value="Update"]')
            .setValue('//section/div[2]/table/thead/tr[2]/td[1]/input', 4)
            .setValue('//section/div[2]/table/thead/tr[2]/td[2]/input', `${testBatchID}`)
            .click('//section/div[2]/table/thead/tr[2]/td[3]/button')
            .waitForElementVisible('//section/div[2]/table/tbody/tr[4]/td[2]')
            .getText('//section/div[2]/table/tbody/tr[4]/td[2]', (res) => {
                client.assert.equal(res.value, testBatchID)
            })
            .click('//section/div[2]/table/tbody/tr[4]/td[3]/button')
            .click('//a[@href="/klaus/CoBrands/Details"]')
            .waitForElementNotVisible('//div[@id="CBLoading"]')
            .waitForElementVisible('//ul[@class="adminnav"]/li[3]/a')
            .getText('//ul[@class="adminnav"]/li[3]/a', (res) => {
                client.assert.equal(res.value, testBatchID.toUpperCase());
            })
            
    },

    'Step 3: pasuing and ending': (client) => {
        client
            .pause(300)
            .end()
    }
}