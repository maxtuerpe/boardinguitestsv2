class StopWatch {
	constructor(){
		this.StartMilliseconds = 0;
    	this.ElapsedMilliseconds = 0;
	}
    start(){
    	this.StartMilliseconds = new Date().getTime();
	}
	stop(){
		this.ElapsedMilliseconds = new Date().getTime() - this.StartMilliseconds;
		console.log(this.ElapsedMilliseconds)
	}
}  
const page1Time = new StopWatch();
const page2Time = new StopWatch();
const page3Time = new StopWatch();
const term1Time = new StopWatch();
const term2Time = new StopWatch();
const endTime 	= new StopWatch();



module.exports = {
	'step 1: Get to merchant dashboard': (client) => {
		client 
			.useXpath()
			.url('https://boardingwebapptest.azurewebsites.net/klaus/UserAccount/Login')
			.waitForElementVisible('//input[@id="Email"]')
			.setValue('//input[@id="Email"]', 'smmike@infinicept.com')
			.click('//button[@value="login"]')
			.waitForElementVisible('//input[@id="Password"]')
			.setValue('//input[@id="Password"]', 'Mike1!')
			.click('//button[@class="btn btn-primary"]')
	},
	'Step 2: Start applicationa and click through steps': (client) => {
		client
			.useXpath()
			.click('//a[@class="btn btn-max"]')
			page1Time.start()
		client
			.waitForElementNotVisible('//div[@id="MAIOLoading"]', () => {
				page1Time.stop()
			})
			.click('//select[@data-shortname="Business Type"]')
			.click('//select[@data-shortname="Business Type"]/option[@value="3"]', () => {
				page2Time.start()
			})
			.click('//button[@tabindex="14"]')
			.waitForElementNotVisible('//div[@id="MAIOLoading"]', () => {
				page2Time.stop()
			})
			.setValue('//div[@data-udefid="owner.fullname"]/div[1]/input', 'Mike')
			.setValue('//div[@data-udefid="owner.fullname"]/div[2]/input', 'T')
			.setValue('//div[@data-udefid="owner.fullname"]/div[3]/input', 'Merchant')
			.clearValue('//input[@data-udefid="owner.perc"]')
			.setValue('//input[@data-udefid="owner.perc"]', '100')
			.setValue('//input[@data-udefid="owner.ssn"]', '111111111')
			.click('//div[@class="dayMonthYear"]/div[1]/select/option[@value="11"]')
			.click('//div[@class="dayMonthYear"]/div[2]/select/option[@value="14"]')
			.click('//div[@class="dayMonthYear"]/div[3]/select/option[@value="1990"]')
			.setValue('//div[@data-udefid="owner.address"]/div[2]/input', "123 Yes st")
			.setValue('//div[@data-udefid="owner.address"]/div[4]/input', "Bigly")
			.click('//div[@data-udefid="owner.address"]/div[5]/select/option[@value="NH"]')
			.setValue('//div[@data-udefid="owner.address"]/div[6]/input', "40406")
			.setValue('//input[@data-udefid="owner.phone"]', "7203456789", () => {
				page3Time.start()
			})
			.click('//button[@tabindex="77"]')
			.waitForElementNotVisible('//div[@id="MAIOLoading"]', () => {
				page3Time.stop()
			})
			.setValue('//input[@data-udefid="bank.routingnumber"]', '111111111')
			.setValue('//input[@data-udefid="bank.routingnumber.confirm"]', '111111111')
			.setValue('//input[@data-udefid="bank.acctnumber"]', '111111111')
			.setValue('//input[@data-udefid="bank.acctnumber.confirm"]', '111111111', () => {
				term1Time.start()
			})
			.click('//button[@tabindex="6"]')
			.waitForElementNotVisible('//div[@id="MAIOLoading"]', () => {
				term1Time.stop()
				term2Time.start()
			})
			.click('//div[@class="agreeBox clearfix"]/button')
			.waitForElementNotVisible('//div[@id="MAIOLoading"]', () => {
				term2Time.stop()
				endTime.start()
			})
			.click('//div[@class="agreeBox clearfix"]/button')
			.waitForElementNotVisible('//div[@id="MAIOLoading"]', () => {
				endTime.stop()
			})
	},
	'step whatever: Pausing and ending': (client) => {
		client
			.pause(3000)
			.end()
	}
}