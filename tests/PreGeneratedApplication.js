// This test creates a new pre-generataed application, adn asserts that the boarding app goes ahead and takes care of it the way you would expect it to.

module.exports = {
	'Step 1: Get to cobrand': function(client) {
    	client
			.url('https://boardingwebapptest.azurewebsites.net/system/UserAccount/Login')
			.waitForElementVisible('input[name=Email]', 1000)
			.setValue('input[name=Email]', 'testadmin@infinicept.com')
			.click('button[name=submitButton]')
			.waitForElementVisible('input[name=Password]', 1000)
			.setValue('input[name=Password]', 'vRnK$W7C1cr5')
			.click('button[class="btn btn-primary"]')
			.waitForElementVisible('a[href="/system/Clients/Details/23"]')
			.click('a[href="/system/Clients/Details/23"]')
			.click('a[href="/klaus/CoBrands/Details"]')
    },

    'Step 2: Create an application': function(client){
        client
            .useXpath()
            .click('//button[@id="pre-start-merchant-application"]')
            .waitForElementVisible('//input[@name="PSMA_EMAIL"]')
            .setValue('//input[@name="PSMA_EMAIL"]', 'infinitestbot@gmail.com')
            .click('//button[@id="PSMA_CONTINUE"]')
            .waitForElementVisible('//div[@class="popup-box"]')
            .click('//a[@class="close uwdetails-opener"]')
            .assert.containsText('//span[@id="merchant-application-status"]', "Pre-Generated")
    },
    'Step 3: pausing and ending': function(client){
        client
            .pause(300)
            .end()
    }
}